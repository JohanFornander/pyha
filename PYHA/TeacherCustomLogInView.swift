//
//  TeacherCustomLogInView.swift
//  PYHA
//
//  Created by Macbook on 2016-10-27.
//  Copyright © 2016 Johan Fornander. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class TeacherCustomLogInView: UIViewController {
    
    var ref = FIRDatabaseReference()
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var teachersLog: UITextView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = FIRDatabase.database().reference()
        txtField.addTarget(self, action: #selector(UITextFieldDelegate.textFieldShouldEndEditing(_:)), for: UIControlEvents.editingChanged)
        txtField.backgroundColor = UIColor.clear
    
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        label.text = ""
    }
    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        label.text = ""
        return true
    }
    
    
    @IBAction func loginBtn(_ sender: AnyObject) {
        if(txtField.text == ""){
           label.text = "Skriv en klasskod!"
        }
        else{
            self.ref.observeSingleEvent(of: .value, with: { (snapshot) in
                
                for classcode in snapshot.children.allObjects as! [FIRDataSnapshot] {
                    let classCodeInDatabase = classcode.key as String
                    
                    if(classCodeInDatabase == self.txtField.text){
                        
                        let newClass = ["personal log":self.teachersLog.text] as [String : Any]
                        self.ref.child(self.txtField.text!).child("personal log").updateChildValues(newClass)
                        self.performSegue(withIdentifier: "TeacherTableView", sender: self)
                    }
                    else{
                        self.label.text = "Fel kod"
                    }
                }
            }) { (error) in
                print("Error! ",error.localizedDescription)
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "TeacherTableView") {
            let dest = segue.destination as! TeacherTableView
            dest.classCode = self.txtField.text!
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}
