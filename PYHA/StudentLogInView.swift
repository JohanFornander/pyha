//
//  StudentLogInView.swift
//  PYHA
//
//  Created by Macbook on 2016-10-20.
//  Copyright © 2016 Johan Fornander. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class StudentLogInView: UIViewController {
    
    
    @IBOutlet weak var firstNameTxtField: UITextField!
    @IBOutlet weak var lastNameTxtField: UITextField!
    @IBOutlet weak var classCodeTxtField: UITextField!
    @IBOutlet weak var headLabel: UILabel!
    
    var ref = FIRDatabaseReference()
    var autoGeneratedFirebaseKey = ""
    var classCodeInDatabase = ""

    
    override func viewDidLoad() {
        firstNameTxtField.backgroundColor = UIColor.clear
        lastNameTxtField.backgroundColor = UIColor.clear
        classCodeTxtField.backgroundColor = UIColor.clear
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        ref = FIRDatabase.database().reference()
        headLabel.text = "Registrera din närvaro . . ."
        
        
        
        firstNameTxtField.addTarget(self, action: #selector(UITextFieldDelegate.textFieldShouldEndEditing(_:)), for: UIControlEvents.editingChanged)
        lastNameTxtField.addTarget(self, action: #selector(UITextFieldDelegate.textFieldShouldEndEditing(_:)), for: UIControlEvents.editingChanged)
        classCodeTxtField.addTarget(self, action: #selector(UITextFieldDelegate.textFieldShouldEndEditing(_:)), for: UIControlEvents.editingChanged)
    
    }
    
    
    
    @IBAction func nextBtn(_ sender: AnyObject) {
        if(firstNameTxtField.text == ""){
            headLabel.text = "Skriv ditt förnamn"
        }
        else{
            headLabel.text = "Registrera din närvaro . . ."
            if(lastNameTxtField.text == ""){
                headLabel.text = "Skriv ditt efternamn"
            }
            else{
                headLabel.text = "Registrera din närvaro . . ."
                
                let firstname = firstNameTxtField.text
                let lastname = lastNameTxtField.text
                let classCodeTxtField = self.classCodeTxtField.text! as String
                
                self.ref.observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    for classcode in snapshot.children.allObjects as! [FIRDataSnapshot] {
                        self.classCodeInDatabase = classcode.key as String
                        
                        if(self.classCodeInDatabase == classCodeTxtField){
                        
                            self.autoGeneratedFirebaseKey = self.ref.childByAutoId().key
                            
                            let newStudent = ["studentName":firstname!+" "+lastname!,"comprehension":"true","isHandUp":"false", "uniqueID":self.autoGeneratedFirebaseKey] as [String : Any]
                            self.ref.child(classcode.key).child(self.autoGeneratedFirebaseKey).setValue(newStudent)
 
                            self.performSegue(withIdentifier: "studentView", sender: self)
                        }
                        else{
                            self.headLabel.text = "Hittade inte klassen, försök igen"
                        }
                    }
                }) { (error) in
                    print("Error! ",error.localizedDescription)
                    //Hej
                }
            }
        }
    }
    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        headLabel.text = "Registrera din närvaro . . ."
        
        return true
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "studentView") {
            
            let dest = segue.destination as! StudentView
            dest.studentUniqueID = autoGeneratedFirebaseKey
            dest.classCode = self.classCodeInDatabase
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
