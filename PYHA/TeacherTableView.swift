//
//  TeacherTableView.swift
//  PYHA
//
//  Created by Macbook on 2016-10-23.
//  Copyright © 2016 Johan Fornander. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class TeacherTableView: UITableViewController {
    
    var ref = FIRDatabaseReference()
    var classCode = ""
    
    //orkade inte göra en dictionary av dessa arrays..
    var studentIDs = [String]()
    var studentNames = [String]()
    var studentIsHandUp = [String]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = FIRDatabase.database().reference()
        
        self.ref.child(classCode).observeSingleEvent(of: .value, with: { snapshot in
            
            if (snapshot.value is NSNull ) {
                print("not found")
            } else {
                
                var studentDataSnapShots = [FIRDataSnapshot]()
                
                for child in snapshot.children {
                    let snap = child as! FIRDataSnapshot
                    studentDataSnapShots.append(snap)
                }
                
                for child in studentDataSnapShots {
                    let dict = child.value as! NSDictionary
                    
                    if(dict["studentName"] != nil){
                        let studentName = dict["studentName"]!
                        self.studentNames.append(studentName as! String)
                    }
                    if(dict["uniqueID"] != nil){
                        let studentID = dict["uniqueID"]!
                        self.studentIDs.append(studentID as! String)
                    }
                    if(dict["isHandUp"] != nil){
                        let handUp = dict["isHandUp"]!
                        self.studentIsHandUp.append(handUp as! String)
                    }
                }
            }
            self.tableView?.reloadData()
        })
        
        tableView.allowsMultipleSelectionDuringEditing = true
        self.navigationItem.prompt = "Swipa vänster för att ta bort ett namn"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let id = self.studentIDs[indexPath.row]
        self.ref.child(self.classCode).child(id).removeValue()
        self.studentIDs.remove(at: indexPath.row)
        self.studentNames.remove(at: indexPath.row)
        self.studentIsHandUp.remove(at: indexPath.row)
    
        self.tableView?.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.studentNames.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentCell", for: indexPath)
 
        if(self.studentNames.count != 0){
            
            if(self.studentIsHandUp[indexPath.row] == "true"){
                cell.textLabel?.text = self.studentNames[indexPath.row]
            }
            else{
                let id = self.studentIDs[indexPath.row]
                self.ref.child(self.classCode).child(id).removeValue()
                self.studentIDs.remove(at: indexPath.row)
                self.studentNames.remove(at: indexPath.row)
                self.studentIsHandUp.remove(at: indexPath.row)
                
                self.tableView?.reloadData()
            }
        }
        return cell
    }

}
