//
//  StudentView.swift
//  PYHA
//
//  Created by Macbook on 2016-10-22.
//  Copyright © 2016 Johan Fornander. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class StudentView: UIViewController {
    
    var studentUniqueID = ""
    var classCode = ""
    var ref = FIRDatabaseReference()
    var studentIDs = [String]()
    var handIsUp = false
    var comprehension = true
    
    
    @IBOutlet weak var handLabel: UILabel!
    @IBOutlet weak var comprehensionLabel: UILabel!
    @IBOutlet weak var handBtnImage: UIButton!
    @IBOutlet weak var comprehensionBtnImage: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = FIRDatabase.database().reference()
        self.navigationItem.prompt = "Tryck Back för att lämna lektionen"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        comprehensionLabel.text = "Du hänger med på lektionen"
    }
    
    
    //raderar eleven från lektionen om man trycker Back i navigationBar)
    override func viewDidDisappear(_ animated: Bool) {
        self.ref.child(self.classCode).child(self.studentUniqueID).removeValue()
        comprehensionLabel.text = "Du hänger med på lektionen"
    }

    
    
    @IBAction func handsFunction(_ sender: AnyObject) {
        if(self.handIsUp == false){
            let image = UIImage(named: "Hand upp ikon.png")! as UIImage
            handBtnImage.setBackgroundImage(image, for: UIControlState.normal)
            let addHandToQueue = ["isHandUp":"true"] as [String : Any]
            self.ref.child(classCode).child(studentUniqueID).updateChildValues(addHandToQueue)
            self.handIsUp = true
        }
        else{
            let image = UIImage(named: "Hand ner ikon.png")! as UIImage
            handBtnImage.setBackgroundImage(image, for: UIControlState.normal)
            let addHandToQueue = ["isHandUp":""] as [String : Any]
            self.ref.child(classCode).child(studentUniqueID).updateChildValues(addHandToQueue)
            self.handIsUp = false
        }
    }
    
    
    @IBAction func comprehensionFunction(_ sender: AnyObject) {
        if(self.comprehension == false){
            let image = UIImage(named: "Grönt ljus.png")! as UIImage
            comprehensionBtnImage.setBackgroundImage(image, for: UIControlState.normal)
            let addHandToQueue = ["comprehension":"true"] as [String : Any]
            self.ref.child(classCode).child(studentUniqueID).updateChildValues(addHandToQueue)
            comprehensionLabel.text = "Du hänger med på lektionen"
            self.comprehension = true
        }
        else{
            let image = UIImage(named: "Röd ljus.png")! as UIImage
            comprehensionBtnImage.setBackgroundImage(image, for: UIControlState.normal)
            let addHandToQueue = ["comprehension":"false"] as [String : Any]
            self.ref.child(classCode).child(studentUniqueID).updateChildValues(addHandToQueue)
            comprehensionLabel.text = "Du hänger inte med på lektionen"
            self.comprehension = false
        }
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
