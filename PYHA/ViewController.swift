//
//  ViewController.swift
//  PYHAxx
//
//  Created by Macbook on 2016-10-12.
//  Copyright © 2016 Johan Fornander. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIApplicationDelegate{
    

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor.clear
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

