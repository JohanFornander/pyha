//
//  TeacherLogInView.swift
//  PYHA
//
//  Created by Macbook on 2016-10-23.
//  Copyright © 2016 Johan Fornander. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class TeacherLogInView: UIViewController {
    
    var ref = FIRDatabaseReference()
    var todaysClassCode = ""
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var teachersLogTxtView: UITextView!
    @IBOutlet weak var classCodeLabel: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    
    
    override func viewDidAppear(_ animated: Bool) {
        ref = FIRDatabase.database().reference()
        
        descriptionLabel.text = "Var god och ange denna kod till närvarande elever"
        loginBtn.setTitle("Starta lektionen", for: .normal)
        
        var randomInt = Int(arc4random_uniform(9999)+1111)
        self.todaysClassCode = String(randomInt)
            
        self.ref.observeSingleEvent(of: .value, with: { (snapshot) in
                
            for _ in snapshot.children.allObjects as! [FIRDataSnapshot] {
                    
                for classcode in snapshot.children.allObjects as! [FIRDataSnapshot] {
                        
                    let classCodeInDatabase = classcode.key as String
                        
                    if(classCodeInDatabase == self.todaysClassCode){
                        randomInt = Int(arc4random_uniform(9999)+1111)
                        self.todaysClassCode = String(randomInt)
                    }
                }
            }
                
        }) { (error) in
            print("Error! ",error.localizedDescription)
        }
        
        classCodeLabel.text = self.todaysClassCode
        teachersLogTxtView.text = "Egna noteringar..."
    }
    
    
    
    @IBAction func startBtn(_ sender: AnyObject) {
        let newClass = ["personal log":teachersLogTxtView.text] as [String : Any]
        
        //self.ref.child("9123").child("personal log").updateChildValues(newClass)  //debug mode
        self.ref.child(self.todaysClassCode).child("personal log").setValue(newClass)
        self.performSegue(withIdentifier: "TeacherTableView", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "TeacherTableView") {
            let dest = segue.destination as! TeacherTableView
            dest.classCode = self.todaysClassCode
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        //Ifall man vill att läraren tar delete på lektionen vid Back-knappen
        //self.ref.child(self.todaysClassCode).removeValue()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
